import { IProduct } from "./../interfaces/product.interface";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { IItem } from "../interfaces/item.interface";

@Injectable({
  providedIn: "root"
})
export class CartState {
  private readonly _items = new BehaviorSubject<IItem[]>([]);
  readonly items$ = this._items.asObservable();

  get items(): IItem[] {
    return this._items.getValue();
  }

  set items(val: IItem[]) {
    this._items.next(val);
  }

  addToCart(product: IProduct, size: string) {
    let existingItemIndex = this.items.findIndex(
      item => item.product._id == product._id
    );

    if (existingItemIndex > -1) {
      this.items[existingItemIndex].quantity++; 
      this.items = [...this.items];
    } else {
      let item: IItem = {
        product,
        size,
        quantity: 1
      };
      this.items = [...this.items, item];
    }

    console.log("ITEMS", this.items);
  }

  removeFromCart(id: String) {
    this.items = this.items.filter(item => item.product._id === id);
  }
}
