import { IProduct } from "./../interfaces/product.interface";
import { environment } from "./../../environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getProducts(): Promise<IProduct[]> {
    return this.httpClient
      .get<IProduct[]>(`${this.apiUrl}/products`)
      .toPromise();
  }
}
