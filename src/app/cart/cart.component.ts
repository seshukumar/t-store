import { IItem } from './../interfaces/item.interface';
import { Component, OnInit } from '@angular/core';
import { CartState } from '../services/cart-state.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  items: IItem[];

  subTotal:number;

  constructor(private cartState: CartState) { }
  ngOnInit(): void {
    this.cartState.items$.subscribe(() => {
      this.items = this.cartState.items;
      this.subTotal = 0;
      this.items.forEach(item => this.subTotal += item.quantity * item.product.price);
    });
  }

}
