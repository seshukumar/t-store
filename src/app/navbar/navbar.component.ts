import { Component, OnInit } from '@angular/core';
import { CartState } from '../services/cart-state.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  closed = false;
  count: number;

  constructor(private cartState: CartState) { }
  
  ngOnInit(){
    this.cartState.items$.subscribe(() => {
      let numOfItems = 0;
      this.cartState.items.forEach(item => numOfItems += item.quantity);
      this.count = numOfItems;
    });
  }

  toggleCart(el: HTMLElement) {
    if(this.count == 0) {
      alert("please add items to the cart");
      return false;
    }
    this.closed = !this.closed;

    if(this.closed) {
      el.classList.remove('hide-cart');
      el.classList.add('show-cart');
    } else {
      el.classList.remove('show-cart');
      el.classList.add('hide-cart');
    }
  }

}
