import { IProduct, sizes } from "./../interfaces/product.interface";
import { Component, OnInit } from "@angular/core";
import { ProductsService } from "./../services/products.service";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.css"]
})
export class HomePageComponent implements OnInit {
  products: IProduct[];
  productsCopy: IProduct[];
  selectedSizes = [];
  sizes = [sizes.L, sizes.M, sizes.S, sizes.XL, sizes.XS];

  constructor(private productService: ProductsService) {}

  ngOnInit(): void {
    this.productService
      .getProducts()
      .then(products => (this.products = this.productsCopy = products))
      .catch(error => console.log("ERROR", error));
  }

  filterBy(event, size) {
    let checked = event.srcElement.checked;

    if (checked) {
      this.selectedSizes.push(size);
    } else {
      let index = this.selectedSizes.findIndex(
        selectedize => selectedize === size
      );
      if (index > -1) {
        this.selectedSizes.splice(index, 1);
      }
    }
    this.products = [];

    if (this.selectedSizes.length > 0) {
      this.productsCopy.forEach(product => {
        let availableSizes = product.availableSizes;
        let sortedSizes = this.selectedSizes.filter(selectedSize =>
          availableSizes.includes(selectedSize)
        );
        if (sortedSizes.length > 0) {
          this.products.push(product);
        }
      });
    } else {
      this.products = this.productsCopy;
    }
  }

  sort(type: string) {
    if (type === "lowest_to_highest") {
      this.products = this.products.sort((a, b) => a.price - b.price);
    } else if (type === "highest_to_lowest") {
      this.products = this.products.sort((a, b) => b.price - a.price);
    }
  }
}
