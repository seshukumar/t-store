import { Component, OnInit, Input, Output } from "@angular/core";
import { CartState } from '../services/cart-state.service';
import { IProduct } from '../interfaces/product.interface';
import { IItem } from '../interfaces/item.interface';

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.css"]
})
export class ProductComponent implements OnInit {
  @Input() product: IProduct;
  size:string;
  constructor(private cartState: CartState) {}

  ngOnInit(): void {}

  addToCart() {
    if(!this.size){
      alert("Please select size");
      return false;
    }
    this.cartState.addToCart(this.product, this.size);
  }

  updateSize(size: string){
    this.size = size;
  }

}
