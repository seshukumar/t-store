export enum sizes {
  M = "M",
  L = "L",
  XS = "XS",
  S = "S",
  XL = "XL"
}

export interface IProduct {
  _id: string;
  title: string;
  image: string;
  price: number;
  availableSizes: Array<sizes>;
}
