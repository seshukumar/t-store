import { IProduct } from './product.interface';
export interface IItem {
    product: IProduct;
    quantity: number;
    size:string;
}